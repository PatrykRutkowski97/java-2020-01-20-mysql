CREATE TABLE dragons (id INT AUTO_INCREMENT PRIMARY KEY ,name VARCHAR(256), color VARCHAR(256), wings_width smallint);

CREATE TABLE eggs (id INT AUTO_INCREMENT,weight SMALLINT, diameter smallint, dragon INT, ornament INT UNIQUE, FOREIGN KEY (dragon) REFERENCES dragons (id), PRIMARY KEY(id));

CREATE TABLE ornaments (id INT AUTO_INCREMENT PRIMARY KEY, color VARCHAR(256), pattern VARCHAR(256), egg INT UNIQUE, FOREIGN KEY (egg) REFERENCES eggs(id));

ALTER TABLE eggs FOREIGN KEY (ornament) REFERENCES ornaments (id);

CREATE TABLE land (id INT AUTO_INCREMENT, name VARCHAR(256) PRIMARY KEY (id));

CREATE TABLE dragon_land (id_dragon INT, id_land INT, FOREIGN KEY (id_dragon) REFERENCES dragons (id), FOREIGN KEY (id_land) REFERENCES land (id));

INSERT INTO dragons (name,color,wings_width) VALUES ("Dygir","green",200), ("Bondril","red",100), ("Onosse","black",250), ("Chiri","yellow",50), ("Lase","blue",300);

INSERT INTO ornaments (color,pattern,egg) VALUES ("pink","mesh",1), ("black","striped",2), ("yellow","dotted",3), ("blue","dotted",4), ("green","striped",5),("red","mesh",6);

INSERT INTO land (name) VALUES ("Froze"), ("Osiwa"), ("Oscyea"), ("Oclurg");

INSERT INTO dragon_land (id_dragon, id_land) VALUES (1,1), (2,1), (1,2), (3,2), (4,2);

CREATE VIEW dragon_ornaments AS SELECT diameter, weight, color, ornament FROM eggs;

CREATE VIEW wings AS SELECT name FROM dragons WHERE wings_width >= 2 AND weight <= 4;

SELECT * FROM dragons;

SELECT * FROM eggs;

SELECT * FROM ornaments;

SELECT * FROM land;

SELECT * FROM dragon_land;

SELECT * FROM dragon_ornaments;

SELECT * FROM wings;

DROP TABLE dragons;

DROP TABLE eggs;

DROP TABLE ornaments;

DROP TABLE land;

DROP TABLE dragon_land;